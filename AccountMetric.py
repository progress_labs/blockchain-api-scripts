#!/usr/local/bin/python3
__author__ = "Tim Fagergren"

import requests
import ConfigLoader

config = ConfigLoader.load_config()

url = f"https://{config['api_domain']}/api/accounts"

response = requests.request("GET", url).json()
data = response['data'].sort(key=lambda x: x['balance'], reverse=True)

print(len(response['data']))

with open("account_hotspots", "a") as file:
    for value in response['data']:
        account_url = f"https://explorer.helium.foundation/api/accounts/{value['address']}/gateways"
        account_response = requests.request("GET", account_url).json()
        hotspot_counts = len(account_response['data'])
        file.write(f"HNT: {int(value['balance']) / config['bones']}, Address: {value['address']}: "
                   f"Hotspot Count: {hotspot_counts}\n")
        print(f"HNT: {int(value['balance']) / config['bones']}, Address: {value['address']}: "
              f"Hotspot Count: {hotspot_counts}")
