FROM python:3.7-slim as build

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

RUN sed -i 's/replace_with_your_account_id/13jFKYu3ywLHzJxdTMDENqnutTo5cEdLs8cQGYeatQSkhWYbDUc/' config.yaml.renameMe
RUN mv config.yaml.renameMe config.yaml



CMD ["python", "AccountTransactions.py"]