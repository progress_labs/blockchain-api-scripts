#!/usr/local/bin/python3
__author__ = "Tim Fagergren"

import requests
import ConfigLoader

config = ConfigLoader.load_config()

url = f"https://{config['api_domain']}/api/accounts"

response = requests.request("GET", f"{url}/{config['account_id']}/transactions").json()

print(f"Total Transactions: {len(response['data'])}")
group_by_type = {
    "poc_challengers_reward": [],
    "poc_challengees_reward": [],
    "poc_witnesses_reward": [],
    "gateway": [],
    "location": []
}
for transaction in response['data']: group_by_type[transaction['type']].append(transaction)

for transaction_type, values in group_by_type.items():
    total = 0
    fee = 0
    for value in values:
        try: total += value["amount"]
        except Exception as error:
            fee += value["fee"]
    print(f"{transaction_type}: {len(group_by_type[transaction_type])}")
    print(f"-- Reward: {total / config['bones']} HNT Fees:{fee / config['bones']} HNT")

