__author__ = "Tim Fagergren"

import yaml

def load_config():
    with open(r'config.yaml') as config_file:
        return(yaml.load(config_file, Loader=yaml.FullLoader))