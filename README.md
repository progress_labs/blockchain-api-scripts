# blockchain-api-scripts
This repository contains scripts for the Helium Network API

## Prerequisites

- Python 3.7
- pip

## Usage

- Rename `config.yaml.renameMe` to `config.yaml`
- Update `config.yaml` with your account id 
    - can be found in helium app
- Run `pip install -r requirements.txt` to install prerequisite python libraries
- Execute scripts using python
     - e.g. `python AccountTransactions.py`
   
## Testing

For now, basic testing is done leveraging docker.

Simply run `docker build .` and then `docker run <buildId>` to test.

## Donations
Feeling some altruism? Here is my Helium Account ID:

HNT: 13jFKYu3ywLHzJxdTMDENqnutTo5cEdLs8cQGYeatQSkhWYbDUc 